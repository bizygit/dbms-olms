use olms;

CREATE TABLE `olms`.`lecture_details` ( 
`lecture_id` INT(11) NOT NULL AUTO_INCREMENT , 
`name` VARCHAR(100) NOT NULL ,
 `user_name` VARCHAR(50) NOT NULL , 
 `password` VARCHAR(250) NOT NULL ,
 `email` VARCHAR(100) NULL DEFAULT NULL ,
 `contact_no` INT(11) NULL DEFAULT NULL , 
 `qualification` VARCHAR(250) NULL DEFAULT NULL , 
 `age` INT(11) NULL DEFAULT NULL ,
 `gender` VARCHAR(20) NULL DEFAULT NULL ,
 `country` VARCHAR(100) NULL DEFAULT NULL ,
 `address` VARCHAR(250) NULL DEFAULT NULL ,
 `experience` INT(20) NULL DEFAULT NULL ,
 `role` VARCHAR(100) NULL DEFAULT NULL ,
 `timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
 PRIMARY KEY (`lecture_id`)) ENGINE = InnoDB;
 
 
 CREATE TABLE `olms`.`lecture_specialization` (
 `lecture_id` INT(11) NOT NULL ,
 `specialization` VARCHAR(100) NULL DEFAULT NULL ) ENGINE = InnoDB;
 
 
 ALTER TABLE `lecture_specialization` 
 ADD FOREIGN KEY (`lecture_id`) 
 REFERENCES `lecture_details`(`lecture_id`) 
 ON DELETE RESTRICT ON UPDATE RESTRICT;
 
 
RENAME TABLE `olms`.`lecture specialization` TO `olms`.`lecture_specialization`;


CREATE TABLE `olms`.`lecture_classes` (
 `course_id` INT(11) NOT NULL AUTO_INCREMENT ,
 `lecture_id` INT(11) NOT NULL ,
 `course_name` VARCHAR(250) NULL DEFAULT NULL ,
 `duration` INT(11) NULL DEFAULT NULL ,
 `course_fee` INT(11) NULL DEFAULT NULL , 
 PRIMARY KEY (`course_id`)) ENGINE = InnoDB;
 
 
 ALTER TABLE `lecture_classes`
 ADD FOREIGN KEY (`lecture_id`)
 REFERENCES `lecture_details`(`lecture_id`) 
 ON DELETE RESTRICT ON UPDATE RESTRICT;
 
 
 CREATE TABLE `olms`.`learner_details` ( 
 `student_id` INT(11) NOT NULL AUTO_INCREMENT ,
 `name` VARCHAR(250) NOT NULL ,
 `user_name` VARCHAR(100) NOT NULL , 
 `password` VARCHAR(250) NOT NULL ,
 `email` VARCHAR(250) NULL DEFAULT NULL ,
 `contact_number` INT(11) NULL DEFAULT NULL ,
 `qualification` VARCHAR(250) NULL DEFAULT NULL , 
 `age` INT(11) NULL DEFAULT NULL ,
 `gender` VARCHAR(50) NULL DEFAULT NULL ,
 `country` VARCHAR(100) NULL DEFAULT NULL ,
 `address` VARCHAR(250) NULL DEFAULT NULL ,
 `experience` INT(11) NULL DEFAULT NULL ,
 `role` VARCHAR(100) NULL DEFAULT NULL , 
 `timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
 PRIMARY KEY (`student_id`)) ENGINE = InnoDB;
 
 
 CREATE TABLE `olms`.`learner_status` (
 `student_id` INT(11) NOT NULL ,
 `course_id` INT(11) NOT NULL ,
 `course_name` VARCHAR(250) NOT NULL ,
 `course_enrolled_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
 `course_progress_hour` INT(11) NOT NULL DEFAULT '0' ,
 `completed` INT(11) NOT NULL DEFAULT '0' ,
 `certificate` INT(11) NOT NULL DEFAULT '0' ,
 `completed_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ) ENGINE = InnoDB;
 
 
 ALTER TABLE `learner_status`
 ADD FOREIGN KEY (`student_id`)
 REFERENCES `learner_details`(`student_id`)
 ON DELETE RESTRICT ON UPDATE RESTRICT;
 
 
 ALTER TABLE `learner_status`
 ADD FOREIGN KEY (`course_id`)
 REFERENCES `lecture_classes`(`course_id`)
 ON DELETE RESTRICT ON UPDATE RESTRICT;
 
 
 CREATE TABLE `olms`.`learner_membership` (
 `student_id` INT(11) NOT NULL ,
 `course_id` INT(11) NOT NULL , 
 `payment_amount` INT(11) NULL DEFAULT NULL ,
 `payment_status` VARCHAR(100) NULL DEFAULT NULL ,
 `payment_method` VARCHAR(100) NULL DEFAULT NULL ,
 `timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ) ENGINE = InnoDB;
 
 
 ALTER TABLE `learner_membership` 
 ADD FOREIGN KEY (`student_id`)
 REFERENCES `learner_details`(`student_id`) 
 ON DELETE RESTRICT ON UPDATE RESTRICT; 
 
 
 ALTER TABLE `learner_membership` 
 ADD FOREIGN KEY (`course_id`)
 REFERENCES `lecture_classes`(`course_id`) 
 ON DELETE RESTRICT ON UPDATE RESTRICT;
 
 
 

 
 
