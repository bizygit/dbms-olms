-- use vedant;

-- select * from employee;

-- DELIMITER $$
-- CREATE PROCEDURE employee_records()
-- BEGIN
--	   select * from employee;
-- END$$
-- DELIMITER ;


-- CREATE VIEW employee_records AS
-- select * from employee;

-- DELIMITER ##
-- CREATE PROCEDURE emp_salary_details()
-- BEGIN
-- 	select count(name), max(salary), min(salary), avg(salary), sum(salary) from employee_1;
-- END##
-- DELIMITER ;

-- DELIMITER ##
-- CREATE PROCEDURE emp_salary_details_by_dept()
-- BEGIN
-- 	select dept, count(name), max(salary), min(salary), avg(salary), sum(salary) 
--     from employee_1 
--     group by dept;
-- END##
-- DELIMITER ;

-- DELIMITER ##
-- CREATE PROCEDURE emp_job_title(IN jobtitle varchar(50))
-- BEGIN
-- 	select name, job_title
--     from employee_1 
--     where job_title = jobtitle;
-- END##
-- DELIMITER ;

-- DELIMITER ##
-- CREATE PROCEDURE emp_salary_calculation(IN empId int(11))
-- BEGIN
-- 	select id as emp_id, salary as monthly_salary, salary*6 as half_yearly_salary,
--     salary*12 as annual_salary
--     from employee_1 
--     where id = empId;
-- END##
-- DELIMITER ;


-- create table stu_report(tid int(4) primary key auto_increment, name varchar(30),
-- subj1 int(2), subj2 int(2), subj3 int(2), total int(3) default 0, average int(3) default 0);

-- create table stu_report_backup as (select * from stu_report);

-- DELIMITER ##
-- CREATE TRIGGER after_stu_report_backup_insert
-- 	AFTER INSERT ON stu_report_backup 
--     FOR EACH ROW
-- BEGIN
--     insert into stu_report (name, subj1, subj2, subj3, total, average) values (
--     new.name, new.subj1, new.subj2, new.subj3,
--     new.subj1 + new.subj2 + new.subj3,
--     (new.subj1 + new.subj2 + new.subj3) / 3);
-- END##
-- DELIMITER ;
--     

-- CREATE TABLE `vedant`.`customer_1` (
--  `Cname` VARCHAR(50) NOT NULL ,
--  `creditlimit` INT(11) NOT NULL ,
--  `category` VARCHAR(50) NOT NULL ) ENGINE = InnoDB;
--  
 

-- delimiter #
-- create procedure customer_category_check()
-- begin
--   declare done int default false;
--   declare x int;
--   declare cur1 cursor for select creditlimit from customer;
--   declare continue handler for not found set done = true;
--   open cur1;
--   read_loop:LOOP
--   fetch cur1 into x;
--   if done then
--   leave read_loop;
--   end if;
--   if x > 50000 then
--   insert into customer_1 values ((select Cname from customer where creditlimit = x), x, 'PLATINUM'); 
--   elseif x <= 50000 and x > 30000 then
--   insert into customer_1 values ((select Cname from customer where creditlimit = x), x, 'GOLD');
--   elseif x <= 30000 and x > 10000 then
--   insert into customer_1 values ((select Cname from customer where creditlimit = x), x, 'SILVER');
--   else
--   insert into customer_1 values ((select Cname from customer where creditlimit = x), x, 'BRONZE');
--   end if;
--   end LOOP;
--   close cur1;
--   end
--   #

  
-- ======================================================================================
use employeeinfo;

-- DELIMITER ##
-- CREATE FUNCTION get_increased_salary(emp_name varchar(10))
-- RETURNS int(11)
-- BEGIN
-- 	DECLARE jobtitle varchar(10);
--     DECLARE new_salary int(11);
--     DECLARE current_salary int(11);
--     SET current_salary = (select annualSalary from emp_1 where name = emp_name);
--     SET jobtitle = (select job_title from emp_1 where name = emp_name);
--     case jobtitle
-- 		when 'manager' then
-- 			SET new_salary =  current_salary + (0.05 * current_salary);
--         when 'clerk' then
-- 			SET new_salary =  current_salary + (0.03 * current_salary);
-- 		else
-- 			SET new_salary = current_salary + (0.02 * current_salary);
--         end case;
--         RETURN new_salary;
-- END##
-- DELIMITER ;

-- desc emp_1;
-- get_increased_salaryselect * from emp_1;
-- alter table emp_1 add column grade varchar(10);

-- DELIMITER ##
-- CREATE FUNCTION assign_grade_emp()
-- RETURNS varchar(50)
-- BEGIN
-- 	update emp_1 set grade = 'D' where annualSalary between 0 and 1000;
--     update emp_1 set grade = 'C' where annualSalary between 1000 and 2000;
--     update emp_1 set grade = 'B' where annualSalary between 2000 and 3500;
--     update emp_1 set grade = 'A' where annualSalary between 3500 and 5000;
--     update emp_1 set grade = 'O' where annualSalary > 5000;

-- 	RETURN "SUCCESS";
-- END##
-- DELIMITER ;


-- CREATE TABLE `customer` ( 
-- `cust_id` INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
--  `name` VARCHAR(50) NOT NULL ,
--  `amount` INT(11) NOT NULL ,
--  `address` VARCHAR(250) NOT NULL);

-- CREATE TABLE `artist` ( 
--  `name` VARCHAR(100) NOT NULL ,
--  `birthplace` VARCHAR(100) NOT NULL ,
--  `age` INT(11) NOT NULL , 
--  `style` VARCHAR(100) NOT NULL );


--  CREATE TABLE `artwork`( 
--   `title` VARCHAR(100) NOT NULL , 
--   `year` DATE NOT NULL ,
--   `type` VARCHAR(50) NOT NULL , 
--   `price` INT(11) NOT NULL); 


-- CREATE TABLE `group` ( 
-- `name` VARCHAR(100) NOT NULL );


-- CREATE TABLE `organization`.`department` ( 
-- `dept_no` INT(11) NOT NULL AUTO_INCREMENT , 
-- `dept_name` VARCHAR(50) NOT NULL ,
--  `dept_head` VARCHAR(50) NOT NULL ,
--  PRIMARY KEY (`dept_no`)) ENGINE = InnoDB;
--  
--  
--  CREATE TABLE `organization`.`dept_mgr_start_date` (
--  `dept_no` INT(11) NOT NULL , 
--  `dept_head` VARCHAR(50) NOT NULL ,
--  `dept_head_start_date` DATE NOT NULL,
--  `dept_head_exit_date` DATE NOT NULL) ENGINE = InnoDB;
--  
--  
--  ALTER TABLE `dept_mgr_start_date`
--  ADD FOREIGN KEY (`dept_no`) 
--  REFERENCES `department`(`dept_no`)
--  ON DELETE RESTRICT ON UPDATE RESTRICT;
--  
--  
--  CREATE TABLE `organization`.`projects` (
--  `proj_no` INT(11) NOT NULL AUTO_INCREMENT ,
--  `dept_no` INT(11) NOT NULL , 
--  `proj_name` VARCHAR(100) NOT NULL , 
--  `proj_location` VARCHAR(100) NOT NULL ,
--  PRIMARY KEY (`proj_no`)) ENGINE = InnoDB;
--  
--  
--  ALTER TABLE `projects`
--  ADD FOREIGN KEY (`dept_no`)
--  REFERENCES `department`(`dept_no`)
--  ON DELETE RESTRICT ON UPDATE RESTRICT;
--  
--  
--  CREATE TABLE `organization`.`employee` ( 
--  `emp_id` INT(11) NOT NULL AUTO_INCREMENT , 
--  `emp_dept_no` INT(11) NOT NULL ,
--  `emp_name` VARCHAR(100) NOT NULL ,
--  `emp_pan_number` INT(11) NOT NULL ,
--  `emp_address` VARCHAR(100) NOT NULL ,
--  `emp_salary` INT(11) NOT NULL ,
--  `emp_gender` VARCHAR(50) NOT NULL ,
--  `emp_birthdate` DATE NOT NULL ,
--  `emp_supervisor` VARCHAR(100) NOT NULL , 
--  PRIMARY KEY (`emp_id`)) ENGINE = InnoDB;
--  
--  
--  ALTER TABLE `employee`
--  ADD FOREIGN KEY (`emp_dept_no`)
--  REFERENCES `department`(`dept_no`)
--  ON DELETE RESTRICT ON UPDATE RESTRICT;
--  
--  
--  CREATE TABLE `organization`.`emp_status` (
--  `emp_status_id` INT(11) NOT NULL AUTO_INCREMENT ,
--  `emp_id` INT(11) NOT NULL ,
--  `proj_no` INT(11) NOT NULL ,
--  `working_status` VARCHAR(100) NOT NULL , 
--  PRIMARY KEY (`emp_status_id`)) ENGINE = InnoDB;
--  
--  ALTER TABLE `emp_status`
--  ADD FOREIGN KEY (`emp_id`)
--  REFERENCES `employee`(`emp_id`) 
--  ON DELETE RESTRICT ON UPDATE RESTRICT;
--  
--  
--  ALTER TABLE `emp_status`
--  ADD FOREIGN KEY (`proj_no`)
--  REFERENCES `projects`(`proj_no`)
--  ON DELETE RESTRICT ON UPDATE RESTRICT;
--  
--  
--  CREATE TABLE `organization`.`working_hour` (
--  `emp_status_id` INT(11) NOT NULL ,
--  `month` VARCHAR(50) NOT NULL ,
--  `week1_work_hour` INT(11) NOT NULL , 
--  `week2_work_hour` INT(11) NOT NULL ,
--  `week3_work_hour` INT(11) NOT NULL ,
--  `week4_work_hour` INT(11) NOT NULL ,
--  `week5_work_hour` INT(11) NOT NULL ) ENGINE = InnoDB;
--  
--  
--  ALTER TABLE `working_hour` 
--  ADD FOREIGN KEY (`emp_status_id`)
--  REFERENCES `emp_status`(`emp_status_id`)
--  ON DELETE RESTRICT ON UPDATE RESTRICT;


